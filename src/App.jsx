// import { useState } from 'react'
import './App.css'


/// Containers ///
import Home from './containers/home'
import Detail from './containers/detail'

/// Components ///
import Header from './components/header'
import Footer from './components/footer'

import {Routes, Route, Navigate} from 'react-router-dom'


function App() {

  return (
    <div>
    <Header />
      <main>
      <Routes>
        <Route exact path ="/" element={<Home />}/>
        <Route exact path="/details/:id" element={<Detail />}/>
        <Route exact path="*" element={<Navigate to="/"/>}/>
      </Routes>
      </main>
    <Footer />
    </div>
  )
}

export default App
