import { SearchMoviesWithKeyWord } from "../api/moviedb";
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'; // Assurez-vous d'importer Link depuis react-router-dom

const Home = (props) => {

  const [keyword, setKeyword] = useState('');
  const [queryResults, setQueryResults] = useState([]);

  useEffect(() => {
    SearchMoviesWithKeyWord(keyword) // Mettez à jour l'appel à SearchMoviesWithKeyWord en passant setQueryResults
    .then((res) => {
      setQueryResults(res)
    })
    .catch((err) => {
      console.log(err)
    })
  }, [keyword, queryResults])

  return (
    <div className="center">
      <h2>MOVIE SEARCH</h2>
      <input type="text" value={keyword} onChange={(e) => setKeyword(e.target.value)} />
      {queryResults.length > 0 && (
        <ul>
          {queryResults.map((movie) => (
            <li key={movie.id}>
              <Link to={`/details/${movie.id}`}>{movie.title}</Link>
            </li>
          ))}
        </ul>
      )}
      {queryResults.length === 0 && (
        <p>Aucun resultat</p>
      )}
    </div>
  );
}

export default Home;
