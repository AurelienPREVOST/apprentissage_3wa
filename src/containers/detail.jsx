import { onLoadGetMovie } from "../api/moviedb"
import { getCompanyDetails } from "../api/moviedb"
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const Detail = (props) => {
  const params = useParams()
  const id = params.id
  const [movie, setMovie] = useState(null)
  const [company, setCompany] = useState(null)

  useEffect(() => {
    onLoadGetMovie(id)
      .then((res) => {
        setMovie(res)
      })
      .catch((err) => {
        console.log(`.CATCH - Erreur id:${id}`)
      })

    getCompanyDetails(id)
      .then((res) => {
        setCompany(res)
      })
      .catch((err) => {
        console.log(`%c .CATCH - Erreur dans getCompanyDetails ${id} - fichier useEffect de detail.jsx`, "color:red")
      })
  }, [])

  // console.log("Valeur de company :", company) // retourne NULL

  return (
    <div className="center">
      {movie !== null &&
      <div>
        <img src={`https://image.tmdb.org/t/p/w200${movie.poster_path}`} />
        <h2>{movie.title}</h2>
      </div>
      }
      <p>Produit par {company}</p>
    </div>
  );
}

export default Detail;
