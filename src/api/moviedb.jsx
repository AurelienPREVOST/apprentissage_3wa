import axios from 'axios'

const API_KEY = 'a1b05bedf7eefc256a73901db96fc0f1';

export function SearchMoviesWithKeyWord(keyword){
    return axios.get(`https://api.themoviedb.org/3/search/movie?query=${keyword}&api_key=${API_KEY}&language=fr-FR`)
    .then(response => {
        let queryResults = response.data.results.slice(0, 10);
        return queryResults;
      })
      .catch(err => {
        console.log(err);
      });

}

export function onLoadGetMovie(id){
  return axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}`)
  .then(response => {
    const movie = response.data
    console.log("movie=>", movie)
    console.log("movie.title =>", movie.title)
    return movie;
  })
  .catch(err => {
    console.log(err);
  });
}

export function getCompanyDetails(id){
    // l'api fonctionne : exemple : https://api.themoviedb.org/3/company/24?api_key=a1b05bedf7eefc256a73901db96fc0f1
    return axios.get(`https://api.themoviedb.org/3/company/${id}?api_key=${API_KEY}`)
    .then((response) => {
      const company = response.data.name
      return company
    })
    .catch((error) => {
      console.log("%c .CATCH JE SUIS L'ERREUR DE GETCOMPANYDETAILS(id) - fichier moviedb", "color:red");
    });
}

/* ---------------------------------------------- */
/*import {onLoadGetMovie} from '../api/moviedb.jsx'

const Bidon = (props) =>{

    const [fake, setFake] = useState(null)
    useEffect(()=> {
        onLoadGetMovie(req.params.id)
        .then((res)=>{
            if(res.status === 200){
                setFake(res)
            }else {
                console.log(res)
            }
        })
        .catch(err=>console.log(err))
    }, [])
    return (<section>
        <h1>Je suis fake et bidon</h1>
    </section>)
}

export default Bidon
*/
